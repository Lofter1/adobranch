package main

import (
	"bytes"
	"context"
	"fmt"
	"strings"
	"text/template"
	"time"

	"github.com/ettle/strcase"
	"github.com/microsoft/azure-devops-go-api/azuredevops/workitemtracking"
)

type workItem struct {
	Id                int
	Title             string
	Description       string
	Type              string
	CreatedBy         user
	ChangedBy         user
	TeamProject       string
	IterationPath     string
	AssignedTo        user
	State             string
	StateChangeReason string
}

type user struct {
	DisplayName string
	UniqueName  string
}

type branchTemplateData struct {
	WorkItem    workItem
	CurrentDate time.Time
}

func (wi workItem) createBranch(nameTemplate string) (string, error) {
	templateFuncMap := template.FuncMap{
		"ToCamel":  strcase.ToCamel,
		"ToPascal": strcase.ToPascal,
	}

	branchTemplate, err := template.New("branchName").Funcs(templateFuncMap).Parse(nameTemplate)
	if err != nil {
		return "", err
	}

	var result bytes.Buffer
	templateData := branchTemplateData{wi, time.Now()}
	err = branchTemplate.Execute(&result, templateData)
	if err != nil {
		return "", err
	}

	resultStr := strings.Replace(result.String(), ":", "", -1)

	return resultStr, nil
}

func branchFromWorkItem(wi workItem, baseBranch string, pull bool, branchNameTemplate string, dry bool) error {
	branchName, err := wi.createBranch(branchNameTemplate)
	if err != nil {
		return err
	}

	if dry {
		fmt.Println(branchName)
		return nil
	}

	if err := gitCheckout(baseBranch); err != nil {
		return err
	}

	if pull {
		if err := gitPull(); err != nil {
			return err
		}
	}

	if err := gitCreateBranch(branchName); err != nil {
		return err
	}
	return nil
}

func getWorkItemById(client workitemtracking.Client, context context.Context, id int) (*workItem, error) {
	workItemArgs := workitemtracking.GetWorkItemArgs{Id: &id}

	wi, err := client.GetWorkItem(context, workItemArgs)
	if err != nil {
		return nil, err
	}

	return convertWorkItem(wi), nil
}

func convertWorkItem(w *workitemtracking.WorkItem) *workItem {
	return &workItem{
		Id:                *w.Id,
		Title:             (*w.Fields)["System.Title"].(string),
		Description:       (*w.Fields)["System.Description"].(string),
		Type:              (*w.Fields)["System.WorkItemType"].(string),
		CreatedBy:         convertUser((*w.Fields)["System.CreatedBy"].(map[string]interface{})),
		ChangedBy:         convertUser((*w.Fields)["System.ChangedBy"].(map[string]interface{})),
		TeamProject:       (*w.Fields)["System.TeamProject"].(string),
		IterationPath:     (*w.Fields)["System.IterationPath"].(string),
		State:             (*w.Fields)["System.State"].(string),
		StateChangeReason: (*w.Fields)["System.Reason"].(string),
	}
}

func convertUser(userDto map[string]interface{}) user {
	return user{
		DisplayName: userDto["displayName"].(string),
		UniqueName:  userDto["uniqueName"].(string),
	}
}
