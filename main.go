package main

import (
	"context"
	"flag"
	"gitlab.com/Lofter1/adobranch/configuration"
	"log"
	"strconv"

	"github.com/microsoft/azure-devops-go-api/azuredevops"
	"github.com/microsoft/azure-devops-go-api/azuredevops/workitemtracking"
)

func init() {
	configuration.SetUsageMessage()
	configuration.InitFlags()
	err := configuration.ParseArgument()
	if err != nil {
		log.Fatal(err)
	}
}

func main() {
	if err := run(); err != nil {
		log.Fatal(err)
	}
}

func run() error {
	connection := azuredevops.NewPatConnection(configuration.Config.OrganizationUrl, configuration.Config.AccessToken)
	ctx := context.Background()
	client := azuredevops.NewClient(connection, configuration.Config.OrganizationUrl)
	workItemClient := workitemtracking.ClientImpl{Client: *client}

	workItemId, err := strconv.Atoi(flag.Args()[0])
	if err != nil {
		return err
	}

	wi, err := getWorkItemById(&workItemClient, ctx, workItemId)
	if err != nil {
		return err
	}

	err = branchFromWorkItem(
		*wi,
		configuration.Config.BaseBranch,
		configuration.Config.Pull,
		configuration.Config.BranchNameTemplate,
		configuration.Config.DryRun)
	if err != nil {
		return err
	}
	return nil
}
