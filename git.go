package main

import (
	"fmt"
	"os/exec"
)

func gitCheckout(target string) error {
	output, err := exec.Command("git", "checkout", target).CombinedOutput()
	fmt.Printf("git: %s\n", output)
	if err != nil {
		return err
	}
	return nil
}

func gitPull() error {
	output, err := exec.Command("git", "pull").CombinedOutput()
	fmt.Printf("git: %s\n", output)
	if err != nil {
		return err
	}
	return nil
}

func gitCreateBranch(name string) error {
	output, err := exec.Command("git", "checkout", "-B", name).CombinedOutput()
	fmt.Printf("git: %s\n", output)
	if err != nil {
		return err
	}
	return nil
}
