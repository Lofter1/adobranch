# ADO Branching
Automated branching for Azure DevOps based on work items

## Installation

### Via Go
Your Go bin folder should be in your path
```bash
go install gitlab.com/Lofter1/adobranch@latest
```

## Usage

```bash
adobranch [flags] <workitem id>
```

### Flags

    -accessToken string
        Your personal access token for Azure DevOps
    -base string
        The branch you want to branch out from (default "main")
    -dry
        See what branch would be created
    -organizationUrl string
        Your Azure DevOps organization/base url (example: https://dev.azure.com/mycompany/)
    -pull
        Sets whether or not a pull should be executed before branching
    -template string
        :The branch template (using the Go templating syntax) (default "WorkItem{{.Id}}_{{.Title | ToPascal}}")

### Environment Variables

    ADOB_ORGANIZATION_URL - define a default for organizationUrl
    ADOB_ACCESS_TOKEN - define a default for accessToken

Example
```bash
adobranch --accessToken <your access token> --organizationUrl https://dev.azure.com/mycompany/ --pull 3948
```

### Branch Template

The template follows the [Go text templating syntax](https://pkg.go.dev/text/template).

The following **fields** are available

| Name                             | Description                                                                     |
|----------------------------------|---------------------------------------------------------------------------------|
| .WorkItem.Id                     | The work item id                                                                |
| .WorkItem.Title                  | The work item title                                                             |
| .WorkItem.Description            | The work item description                                                       |
| .WorkItem.Type                   | The work item type                                                              |
| .WorkItem.CreatedBy.DisplayName  | The display name of the creator of the work item                                |
| .WorkItem.CreatedBy.UniqueName   | The unique ADO name of the creator of the work item                             |
| .WorkItem.ChangedBy.DisplayName  | The display name of the last person that changed the work item                  |
| .WorkItem.ChangedBy.UniqueName   | The unique ADO name of the last person that changed the work item               |
| .WorkItem.TeamProject            | The team project the work item is located in                                    |
| .WorkItem.IterationPath          | The current iteration of the work item                                          |
| .WorkItem.AssignedTo.DisplayName | The display name of current assignee of the work item                           |
| .WorkItem.AssignedTo.UniqueName  | The unique ADO name of the curren assignee of the work item                     |
| .WorkItem.State                  | The current state of the work item                                              |
| .WorkItem.StateChangeReason      | The reason for the last state change                                            |
| .CurrentDate                     | Time.Now(), see [Go Docs](https://pkg.go.dev/time#Time) for further information |

The following pipes are available

| Name     | Description                                 |
|----------|---------------------------------------------|
| ToCamel  | Converts the specified string to camelCase  |
| ToPascal | Converts the specified string to PascalCase |

#### Formatting Time

Go has a pretty weird way to format time. In order to not always having to look it up, here is a quick reference for the most popular options

| Classic Format | Go Format |
|----------------|-----------|
| yyyy           | 2006      |
| yy             | 06        |
| MMMM           | January   |
| MMM            | Jan       |
| MM             | 01        |
| M              | 1         |
| dd             | 02        |
| d              | 2         |