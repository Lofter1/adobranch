package configuration

import (
	"errors"
	"net/url"
	"text/template"
)

func ValidateConfiguration() error {
	if err := validateOrganizationUrl(Config.OrganizationUrl); err != nil {
		return err
	}
	if err := validateAccessToken(Config.AccessToken); err != nil {
		return err
	}
	if err := validateBaseBranch(Config.BaseBranch); err != nil {
		return err
	}
	if err := validateBranchNameTemplate(Config.BranchNameTemplate); err != nil {
		return nil
	}
	return nil
}

func validateOrganizationUrl(organizationUrl string) error {
	if len(organizationUrl) == 0 {
		return errors.New("organizationUrl is not defined")
	}

	if _, err := url.Parse(organizationUrl); err != nil {
		return err
	}

	return nil
}

func validateAccessToken(accessToken string) error {
	if len(accessToken) == 0 {
		return errors.New("accessToken is not defined")
	}
	return nil
}

func validateBaseBranch(baseBranch string) error {
	if len(baseBranch) == 0 {
		return errors.New("base is not defined")
	}
	return nil
}

func validateBranchNameTemplate(branchNameTemplate string) error {
	if len(branchNameTemplate) == 0 {
		return errors.New("template is not defined")
	}
	if _, err := template.New("").Parse(branchNameTemplate); err != nil {
		return err
	}
	return nil
}
