package configuration

import (
	"flag"
	"fmt"
	"log"
	"os"
)

var Config struct {
	OrganizationUrl    string
	AccessToken        string
	BaseBranch         string
	BranchNameTemplate string
	Pull               bool
	DryRun             bool
}

const envVarPrefix = "ADOB"

func InitFlags() {
	const (
		organizationUrlUsage    = "Your Azure DevOps organization/base url (example: https://dev.azure.com/mycompany/)"
		accessTokenUsage        = "Your personal access token for Azure DevOps"
		baseBranchUsage         = "The branch you want to branch out from"
		branchNameTemplateUsage = "The branch template (using the Go templating syntax)"
		pullUsage               = "Sets whether or not a pull should be executed before branching"
	)

	flag.StringVar(&Config.OrganizationUrl, "organizationUrl", os.Getenv(envVarPrefix+"_ORGANIZATION_URL"), organizationUrlUsage)
	flag.StringVar(&Config.AccessToken, "accessToken", os.Getenv(envVarPrefix+"_ACCESS_TOKEN"), accessTokenUsage)
	flag.StringVar(&Config.BaseBranch, "base", "main", baseBranchUsage)
	flag.StringVar(&Config.BranchNameTemplate, "template", "WorkItem{{.Id}}_{{.Title | ToPascal}}", branchNameTemplateUsage)
	flag.BoolVar(&Config.Pull, "pull", false, pullUsage)
	flag.BoolVar(&Config.DryRun, "dry", false, "See what branch would be created")
}

func SetUsageMessage() {
	flag.Usage = func() {
		_, err := fmt.Fprintf(flag.CommandLine.Output(), "%s [flags] workitemId\n", os.Args[0])
		if err != nil {
			log.Fatal(err)
		}
		flag.PrintDefaults()
	}
}

func ParseArgument() error {
	flag.Parse()

	if flag.NArg() == 0 {
		flag.Usage()
		os.Exit(1)
	}

	return ValidateConfiguration()
}
