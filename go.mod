module gitlab.com/Lofter1/adobranch

go 1.17

require (
	github.com/ettle/strcase v0.1.1
	github.com/microsoft/azure-devops-go-api/azuredevops v1.0.0-b5
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/google/uuid v1.1.1 // indirect
	github.com/stretchr/testify v1.7.0 // indirect
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b // indirect
)
