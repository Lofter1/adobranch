BINARY_NAME=adobranch
OUTPUT_DIR=bin
FLAGS=-ldflags="-s -w"

compile: macos linux windows


macos: macos-intel macos-arm64

macos-arm64:
	GOOS=darwin GOARCH=arm64 
	go build ${FLAGS} -o ${OUTPUT_DIR}/${BINARY_NAME}-macos-arm64 .

macos-intel:
	GOOS=darwin GOARCH=amd64 
	go build ${FLAGS} -o ${OUTPUT_DIR}/${BINARY_NAME}-macos-x64 .


linux: linux-x64 linux-arm64

linux-x64:
	GOOS=linux GOARCH=amd64 
	go build ${FLAGS} -o ${OUTPUT_DIR}/${BINARY_NAME}-linux-x64 .
	
linux-arm64:
	GOOS=linux GOARCH=arm64 
	go build ${FLAGS} -o ${OUTPUT_DIR}/${BINARY_NAME}-linux-arm64 .	


windows:
	GOOS=windows GOARCH=amd64 
	go build ${FLAGS} -o ${OUTPUT_DIR}/${BINARY_NAME}-windows-x64.exe .


clean:
	go clean
	rm -r bin
